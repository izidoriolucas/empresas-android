![N|Solid](logo_ioasys.png)

# README #

### Dados para Execução ###

* Abrir a ferramenta Android Studio, selecionar o módulo 'app' e executar no dispositivo de preferência (emulador ou dispositivo físico).

### Bibliotecas ###

* Biblioteca Glide usada para carregamento de imagens por conta de sua facilidade para realizar a tarefa.
* Bibliotecas Retrofit e Gson pela integração simplificada das duas bibliotecas no consumo de APIs e conversão de JSON.

### Outras Informações ###

* Com mais tempo eu, principalmente, terminaria o ícone do aplicativo, adicionaria suporte à mais informações vindas da API (como dados das páginas no LinkedIn, Facebook e outros), melhoraria o sistema de busca para fazer diretamente no aplicativo (sem outras requisições, apesar de ter escolhido a pesquisa por requisição por ter disponível a API com filtro os documentos recebidos) e adicionaria notificações visuais sobre o andamento das requisições.

