package com.lucas.izidorio.empresas.view

import android.content.res.Resources
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.SearchView
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast
import com.lucas.izidorio.empresas.R
import com.lucas.izidorio.empresas.presenter.MainActivityPresenter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*

class MainActivity : AppCompatActivity(), MainActivityPresenter.View {

    private var presenter: MainActivityPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.supportActionBar?.hide()

        presenter = MainActivityPresenter(this)

        searchView.setOnSearchClickListener {
            presenter?.startSearching()
            presenter?.loadEnterprises(enterprisesList, this)
        }

        searchView.setOnCloseListener {
            presenter?.cancelSearching()
            false
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                presenter?.queryEnterprises(enterprisesList, this@MainActivity, if (query == "") { null } else { query })
                return true
            }

            override fun onQueryTextChange(query: String?): Boolean {
                return true
            }

        })
    }

    override fun showRecyclerView() {
        enterprisesList.visibility = View.VISIBLE
    }

    override fun showSearch() {
        searchView.layoutParams = layoutParamsFromView(searchView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        (searchView.layoutParams as RelativeLayout.LayoutParams).setMargins((27 * Resources.getSystem().displayMetrics.density).toInt(), 0, 0, 0)
        logoNav.visibility = View.GONE
        navSearchViewUnderline.visibility = View.VISIBLE
        searchIcon.visibility = View.VISIBLE
    }

    override fun hideSearch() {
        searchView.layoutParams = layoutParamsFromView(searchView, (54 * Resources.getSystem().displayMetrics.density).toInt(), ViewGroup.LayoutParams.MATCH_PARENT)
        logoNav.visibility = View.VISIBLE
        navSearchViewUnderline.visibility = View.GONE
        searchIcon.visibility = View.GONE
    }

    override fun hideHint() {
        startHint.visibility = View.GONE
    }

    override fun showToast(content: String) {
        Toast.makeText(this, content, Toast.LENGTH_SHORT).show()
    }

    private fun layoutParamsFromView(view: View, width: Int, height: Int): ViewGroup.LayoutParams? {
        val params = view.layoutParams
        params.width = width
        params.height = height
        return params
    }
}
