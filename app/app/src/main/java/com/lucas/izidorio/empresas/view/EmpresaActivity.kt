package com.lucas.izidorio.empresas.view

import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.lucas.izidorio.empresas.R
import com.lucas.izidorio.empresas.presenter.EmpresaActivityPresenter
import kotlinx.android.synthetic.main.activity_empresa.*
import kotlinx.android.synthetic.main.toolbar_empresa.*

class EmpresaActivity : AppCompatActivity(), EmpresaActivityPresenter.View {

    var presenter: EmpresaActivityPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_empresa)

        this.supportActionBar?.hide()

        presenter = EmpresaActivityPresenter(this)

        presenter?.loadEnterpriseData(intent.getIntExtra("id", -1), this)

        backButton.setOnClickListener {
            presenter?.returnButtonClicked()
        }
    }

    override fun getEnterpriseImage(): ImageView {
        return this.imageEmpresa
    }

    override fun setEnterpriseDescription(description: String) {
        this.description.text = description
    }

    override fun setEnterpriseName(name: String) {
        this.empresaTitle.text = name
    }

    override fun finishActivity() {
        this.finish()
    }

    override fun showToast(content: String) {
        Toast.makeText(this, content, Toast.LENGTH_SHORT).show()
    }
}
