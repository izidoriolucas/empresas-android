package com.lucas.izidorio.empresas.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.lucas.izidorio.empresas.R
import com.lucas.izidorio.empresas.model.serialization.EnterpriseDataClasses.Enterprise
import kotlinx.android.synthetic.main.enterprise_item.view.*

class EnterpriseAdapter(private val enterprises: List<Enterprise>,
                        private val context: Context
) : RecyclerView.Adapter<EnterpriseAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val enterprise = enterprises[position]
        holder.bindView(enterprise)
        holder.setOnClickListener {
            if (context is Activity) {
                val intent = Intent(context, EmpresaActivity::class.java)
                intent.putExtra("id", enterprise.id)
                context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.enterprise_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return enterprises.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun setOnClickListener(listener: () -> Unit) {
            itemView.setOnClickListener {
                listener.invoke()
            }
        }

        fun bindView(enterprise: Enterprise) {
            val name = itemView.nomeEmpresa
            val business = itemView.tipoEmpresa
            val local = itemView.localEmpresa
            val image = itemView.imagemEmpresa

            name.text = enterprise.enterprise_name
            business.text = enterprise.enterprise_type.enterprise_type_name
            local.text = enterprise.country

            val url = if (enterprise.photo == "null" || enterprise.photo == null) {
                "https://samuelkleinberg.com/wp-content/uploads/2017/03/no-image.jpg"
            } else {
                "http://empresas.ioasys.com.br${enterprise.photo}"
            }

            Glide.with(itemView)
                .load(url)
                .centerCrop()
                .into(image)
        }

    }

}
