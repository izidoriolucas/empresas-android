package com.lucas.izidorio.empresas.model.retrofit.services

import com.lucas.izidorio.empresas.model.serialization.EnterpriseDataClasses
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface EnterprisesApiService {
    @GET("enterprises")
    fun getEnterprises(@Header("access-token") accessToken: String,
                       @Header("client") client: String,
                       @Header("uid") uid: String): Observable<EnterpriseDataClasses.EnterpriseList>

    @GET("enterprises")
    fun getEnterprises(@Header("access-token") accessToken: String,
                       @Header("client") client: String,
                       @Header("uid") uid: String,
                       @Query("name") name: String): Observable<EnterpriseDataClasses.EnterpriseList>

    @GET("enterprises/{id}")
    fun getEnterprise(@Path("id") id: Int,
                      @Header("access-token") accessToken: String,
                      @Header("client") client: String,
                      @Header("uid") uid: String): Observable<EnterpriseDataClasses.EnterpriseContainer>

    companion object Factory {
        fun create(): EnterprisesApiService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://empresas.ioasys.com.br/api/v1/")
                .build()

            return retrofit.create(EnterprisesApiService::class.java)
        }
    }
}