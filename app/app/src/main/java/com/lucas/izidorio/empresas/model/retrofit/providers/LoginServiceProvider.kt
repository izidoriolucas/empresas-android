package com.lucas.izidorio.empresas.model.retrofit.providers

import com.lucas.izidorio.empresas.model.serialization.UserDataClasses
import com.lucas.izidorio.empresas.model.retrofit.services.LoginApiService
import retrofit2.Call

class LoginServiceProvider(private val loginApiService: LoginApiService) {
    fun login(email: String, password: String): Call<UserDataClasses.Investor> {
        return loginApiService.login(UserDataClasses.UserAuth(email, password))
    }
}