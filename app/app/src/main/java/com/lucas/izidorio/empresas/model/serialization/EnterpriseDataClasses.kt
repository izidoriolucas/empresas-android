package com.lucas.izidorio.empresas.model.serialization

abstract class EnterpriseDataClasses {
    data class EnterpriseList(val enterprises: List<Enterprise>)
    data class EnterpriseContainer(val enterprise: Enterprise)
    data class Enterprise(val id: Int,
                          val enterprise_name: String,
                          val photo: String?,
                          val description: String,
                          val country: String,
                          val enterprise_type: EnterpriseType
    )
    data class EnterpriseType(val id: Int,
                              val enterprise_type_name: String)
}
