package com.lucas.izidorio.empresas.model.serialization

abstract class UserDataClasses {
    data class UserAuth(val email: String,
                        val password: String)
    data class Investor(val data: User)
    data class User(val id: Int,
                    val investor_name: String,
                    val email: String,
                    val city: String)
    class Credentials(val accessToken: String,
                      val client: String,
                      val uid: String)
    object UserHolder {
        var user: User? = null
        var credentials: Credentials? = null
    }
}