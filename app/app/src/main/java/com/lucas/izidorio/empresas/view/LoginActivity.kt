package com.lucas.izidorio.empresas.view

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.widget.Toast
import com.lucas.izidorio.empresas.R
import com.lucas.izidorio.empresas.presenter.LoginActivityPresenter
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginActivityPresenter.View {

    private var presenter: LoginActivityPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        this.supportActionBar?.hide()
        this.window.statusBarColor = ContextCompat.getColor(this, R.color.dark_beige)

        presenter = LoginActivityPresenter(this)

        loginButton.setOnClickListener {
            presenter!!.performLogin()
        }
    }

    override fun startNextActivity() {
        startActivity(Intent(this, MainActivity::class.java)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
    }

    override fun getEmailFieldValue(): String {
        return this.emailInput.text.toString()
    }

    override fun getPasswordFieldValue(): String {
        return this.passwordInput.text.toString()
    }

    override fun showToast(content: String) {
        Toast.makeText(this, content, Toast.LENGTH_SHORT).show()
    }

}
