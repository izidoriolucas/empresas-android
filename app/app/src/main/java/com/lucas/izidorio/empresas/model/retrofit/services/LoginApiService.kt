package com.lucas.izidorio.empresas.model.retrofit.services

import com.lucas.izidorio.empresas.model.serialization.UserDataClasses
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginApiService {
    @POST("users/auth/sign_in")
    fun login(@Body user: UserDataClasses.UserAuth): Call<UserDataClasses.Investor>

    companion object Factory {
        fun create(): LoginApiService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://empresas.ioasys.com.br/api/v1/")
                .build()

            return retrofit.create(LoginApiService::class.java)
        }
    }
}