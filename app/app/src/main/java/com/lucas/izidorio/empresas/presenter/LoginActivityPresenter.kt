package com.lucas.izidorio.empresas.presenter

import android.util.Log
import com.lucas.izidorio.empresas.model.retrofit.services.LoginApiService
import com.lucas.izidorio.empresas.model.retrofit.providers.LoginServiceProvider
import com.lucas.izidorio.empresas.model.serialization.UserDataClasses
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivityPresenter(private val view: View) {

    private val tag = "LoginActivityPresenter"
    private var loginRequestRunning = false

    fun performLogin() {
        if (loginRequestRunning) return
        loginRequestRunning = true
        val email = view.getEmailFieldValue()
        val password = view.getPasswordFieldValue()

        val provider = LoginServiceProvider(LoginApiService.create())
        provider.login(email, password)
            .enqueue(object : Callback<UserDataClasses.Investor> {
                override fun onResponse(call: Call<UserDataClasses.Investor?>?,
                                        response: Response<UserDataClasses.Investor?>?) {
                    when (response?.code()) {
                        200 -> {
                            val user = response.body()?.data
                            UserDataClasses.UserHolder.user = user
                            UserDataClasses.UserHolder.credentials = UserDataClasses.Credentials(
                                response.headers().get("access-token") ?: "",
                                response.headers().get("client") ?: "",
                                response.headers().get("uid") ?: ""
                                )
                            view.startNextActivity()
                        }
                        401 -> view.showToast("Email e/ou senha incorretos")
                        else -> {
                            view.showToast("Falha ao fazer login")
                            Log.e(tag, "Response code: ${response?.code()} | Response: $response")
                        }
                    }
                    loginRequestRunning = false
                }

                override fun onFailure(call: Call<UserDataClasses.Investor?>?, t: Throwable?) {
                    Log.e(tag, t?.message)
                    loginRequestRunning = false
                }
            })
    }

    interface View {
        fun startNextActivity()
        fun getEmailFieldValue(): String
        fun getPasswordFieldValue(): String
        fun showToast(content: String)
    }
}