package com.lucas.izidorio.empresas.presenter

import android.app.Activity
import android.graphics.Bitmap
import android.util.Log
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.lucas.izidorio.empresas.model.retrofit.providers.EnterpriseServiceProvider
import com.lucas.izidorio.empresas.model.retrofit.services.LoginApiService
import com.lucas.izidorio.empresas.model.retrofit.providers.LoginServiceProvider
import com.lucas.izidorio.empresas.model.retrofit.services.EnterprisesApiService
import com.lucas.izidorio.empresas.model.serialization.UserDataClasses
import com.lucas.izidorio.empresas.view.EnterpriseAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EmpresaActivityPresenter(private val view: View) {

    private val tag = "EmpActivityPresenter"

    fun loadEnterpriseData(id: Int, activity: Activity) {
        if (id == -1) {
            Log.d(tag, "Failed to get id")
            return
        }
        val provider = EnterpriseServiceProvider(EnterprisesApiService.create())
        val subscription = provider.getEnterprise(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe ({ result ->
                view.setEnterpriseDescription(result.enterprise.description)
                view.setEnterpriseName(result.enterprise.enterprise_name.toUpperCase())

                val url = if (result.enterprise.photo == "null" || result.enterprise.photo == null) {
                    "https://samuelkleinberg.com/wp-content/uploads/2017/03/no-image.jpg"
                } else {
                    "http://empresas.ioasys.com.br${result.enterprise.photo}"
                }

                Glide.with(activity)
                    .load(url)
                    .centerCrop()
                    .into(view.getEnterpriseImage())
            }, { error ->
                Log.e(tag, "Failure: $error")
                view.showToast("Não foi possível conectar ao servidor")
            })
    }

    fun returnButtonClicked() {
        view.finishActivity()
    }

    interface View {
        fun getEnterpriseImage(): ImageView
        fun setEnterpriseDescription(description: String)
        fun setEnterpriseName(name: String)
        fun finishActivity()
        fun showToast(content: String)
    }
}