package com.lucas.izidorio.empresas.model.retrofit.providers

import com.lucas.izidorio.empresas.model.serialization.EnterpriseDataClasses
import com.lucas.izidorio.empresas.model.serialization.UserDataClasses
import com.lucas.izidorio.empresas.model.retrofit.services.EnterprisesApiService
import io.reactivex.Observable

class EnterpriseServiceProvider(private val enterpriseApiService: EnterprisesApiService) {
    fun listEnterprises(): Observable<EnterpriseDataClasses.EnterpriseList> {
        return enterpriseApiService.getEnterprises(
            UserDataClasses.UserHolder.credentials!!.accessToken,
            UserDataClasses.UserHolder.credentials!!.client,
            UserDataClasses.UserHolder.credentials!!.uid)
    }
    fun listEnterprises(name: String): Observable<EnterpriseDataClasses.EnterpriseList> {
        return enterpriseApiService.getEnterprises(
            UserDataClasses.UserHolder.credentials!!.accessToken,
            UserDataClasses.UserHolder.credentials!!.client,
            UserDataClasses.UserHolder.credentials!!.uid,
            name)
    }
    fun getEnterprise(id: Int): Observable<EnterpriseDataClasses.EnterpriseContainer> {
        return enterpriseApiService.getEnterprise(id,
            UserDataClasses.UserHolder.credentials!!.accessToken,
            UserDataClasses.UserHolder.credentials!!.client,
            UserDataClasses.UserHolder.credentials!!.uid)
    }
}