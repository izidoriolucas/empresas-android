package com.lucas.izidorio.empresas.presenter

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.lucas.izidorio.empresas.model.retrofit.services.EnterprisesApiService
import com.lucas.izidorio.empresas.model.retrofit.providers.EnterpriseServiceProvider
import com.lucas.izidorio.empresas.view.EnterpriseAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainActivityPresenter(private val view: View) {

    private val tag = "MainActivityPresenter"
    private var adapter: EnterpriseAdapter? = null

    fun startSearching() {
        view.showSearch()
        view.hideHint()
    }

    fun cancelSearching() {
        view.hideSearch()
    }

    fun loadEnterprises(recyclerView: RecyclerView, context: Context) {
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
        searchEnterprises(recyclerView, context, null)
    }

    fun queryEnterprises(recyclerView: RecyclerView, context: Context, filter: String?) {
        searchEnterprises(recyclerView, context, filter)
    }

    private fun searchEnterprises(recyclerView: RecyclerView, context: Context, filter: String?) {
        val provider =
            EnterpriseServiceProvider(EnterprisesApiService.create())

        val searchFunction = if (filter == null) {
            provider.listEnterprises()
        } else {
            provider.listEnterprises(filter)
        }

        val subscription = searchFunction
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe ({ result ->
                adapter = EnterpriseAdapter(result.enterprises, context)
                recyclerView.adapter = adapter
                view.showRecyclerView()
            }, { error ->
                Log.e(tag, "Failure: $error")
                view.showToast("Não foi possível conectar ao servidor")
            })
    }

    interface View {
        fun showRecyclerView()
        fun showSearch()
        fun hideSearch()
        fun hideHint()
        fun showToast(content: String)
    }
}